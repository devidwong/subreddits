import { nextTick, h, Suspense, defineComponent } from 'vue'
import { flushPromises, mount } from '@vue/test-utils'

export const element = (selector, component) => component.find(selector)
export const elements = (selector, component) => component.findAll(selector)
export const count = (selector, component) => elements(selector, component).length
export const click = () => ({
  in: (selector, component) => element(selector, component).trigger('click'),
  element: (elementToClick) => elementToClick.trigger('click')
})
export const text = () => ({
  on: (selector, component) => element(selector, component).text()
})
export const change = (ref) => ({
  to (newValue) {
    ref.value = newValue
    return nextTick
  }
})
export const type = (text) => ({
  in: (selector, component) => element(selector, component)
    .setValue(text)
})
export const mountWithSuspense = (component) => async (props, slots) => {
  const SuspenseWrapper = defineComponent({
    props: [...Object.keys(props)],
    setup () {
      return () => h(Suspense, null, {
        default: h(component, { ...props }, { ...slots }),
        fallback: h('div', 'fallback')
      })
    }
  })
  const wrapper = mount(SuspenseWrapper)
  await flushPromises()
  return wrapper
}

export const fetchMockFactory = (dataSuccess) => () => Promise.resolve({
  json: () => Promise.resolve(dataSuccess)
})

export function aPost () {
  const value = {
    post: {}
  }
  return ({
    withId (id) {
      value.post = { ...value.post, id }
      return this
    },
    by (author) {
      value.post = { ...value.post, author }
      return this
    },
    build () {
      return value.post
    }
  })
}

export function aComment () {
  const value = {
    comment: {}
  }
  return ({
    withId (id) {
      value.comment = { ...value.comment, id }
      return this
    },
    by (author) {
      value.comment = { ...value.comment, author }
      return this
    },
    build () {
      return value.comment
    }
  })
}
