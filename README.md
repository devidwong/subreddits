# reddit-viewer in Vue.js 3

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```
- I wanted to define comment template same way as post template – inline in the `app.vue`. For recurring components it was not possible. I discuss it on [https://github.com/vuejs/vue-next/issues/2508][vue-next Github issue] and [https://stackoverflow.com/questions/64572235/is-possible-to-have-recurring-slots-in-vue][StackOverflow question] . StackOverflow user inspired me with the idea to pass comment’s details as a prop. I’ll continue discussion and experiments of recurring slots in the future.

[vue-next Github issue]: https://github.com/vuejs/vue-next/issues/2508

[StackOverflow question]: https://stackoverflow.com/questions/64572235/is-possible-to-have-recurring-slots-in-vue
