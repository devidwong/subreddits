import { ref } from 'vue'
import { useLoading } from '@/utils/useLoading'

export const useRedditComments = (service) => (thread, linkId) => () => {
  const comments = ref([])
  const { loading, setLoading } = useLoading()
  const toggleComments = async () => {
    if (comments.value.length) {
      emptyComments()
    } else {
      setLoading(true)
      setComments(await service(thread, linkId))
      setLoading(false)
    }
  }

  const setComments = (children) => {
    comments.value = children
  }
  const emptyComments = () => {
    comments.value = []
  }

  return {
    comments,
    toggleComments,
    loading
  }
}
