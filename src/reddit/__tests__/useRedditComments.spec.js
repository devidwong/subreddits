import { aComment } from '../../../test/utils'
import { useRedditComments } from '@/reddit/useRedditComments'

const createLoadCommentsMock = (comments) => jest.fn(() => Promise.resolve(comments))
const threadName = 'awwSub'
const postId = 'post0321'
describe('Post comments provider', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  it('loading comments', async () => {
    const commentsToLoad = [
      aComment().withId(2).by('ben').build(),
      aComment().withId(3).by('ann').build()
    ]
    const loadCommentsMock = createLoadCommentsMock(commentsToLoad)
    const { comments, toggleComments } = useRedditComments(loadCommentsMock)(threadName, postId)()

    await toggleComments()

    expect(loadCommentsMock.mock.calls).toEqual([[threadName, postId]])
    expect(comments.value).toEqual(commentsToLoad)
  })
})
