import { useReddit } from '@/reddit/useReddit'

const token = 'TOKEN-abc-012'
const getTokenMock = jest.fn(() => Promise.resolve(token))
const newPostsMock = jest.fn()
const searchMock = jest.fn()
const commentsMock = jest.fn()
const redditServiceMock = {
  getToken: getTokenMock,
  newPosts: newPostsMock,
  search: searchMock,
  comments: commentsMock
}
describe('Reddit functionality hooks provider', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  it('consumes redditService to provide authenticated services for further usage', async () => {
    await useReddit(redditServiceMock)

    expect(getTokenMock.mock.calls).toEqual([[]])
    expect(newPostsMock.mock.calls).toEqual([[token]])
    expect(searchMock.mock.calls).toEqual([[token]])
    expect(commentsMock.mock.calls).toEqual([[token]])
  })
})
