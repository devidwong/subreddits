import { aPost } from '../../../test/utils'
import { useRedditSearch } from '@/reddit/useRedditSearch'

const createLoadSearchResultsMock = (posts, after) => jest.fn(() => Promise.resolve({
  children: posts,
  after
}))
const after = 'lastPostId'
const threadName = 'awwSub'
const searchPhrase = 'not empty'
const setLoadingMock = jest.fn()
const loadMoreWrapper = jest.fn((loadMore) => loadMore)
describe('Subreddit search results posts provider', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  it('loading searchResults', async () => {
    const postSearchResults = [
      aPost().withId(2).by('ben').build(),
      aPost().withId(3).by('ann').build()
    ]
    const loadSearchResultsMock = createLoadSearchResultsMock(postSearchResults, after)
    const { search, searchResults } = useRedditSearch(loadSearchResultsMock)(setLoadingMock, loadMoreWrapper)

    const loadMoreSearchResults = await search(threadName, searchPhrase)

    expect(loadSearchResultsMock.mock.calls).toEqual([[threadName, searchPhrase]])
    expect(searchResults.children).toEqual(postSearchResults)
    expect(setLoadingMock.mock.calls).toEqual([[true], [false]])
    expect(loadMoreWrapper).toHaveBeenCalled()
    jest.clearAllMocks()

    await loadMoreSearchResults()
    await loadMoreSearchResults() // throttled - second call do nothing

    expect(loadSearchResultsMock.mock.calls)
      .toEqual([[threadName, searchPhrase, after]])
    expect(searchResults.children).toEqual([...postSearchResults, ...postSearchResults])
    expect(setLoadingMock.mock.calls).toEqual([[true], [false]])
    expect(loadMoreWrapper).not.toHaveBeenCalled()
  })
})
