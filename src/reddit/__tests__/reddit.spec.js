import Reddit from '@/reddit/reddit'
import { mountWithSuspense } from '../../../test/utils'

const redditServiceMock = {}
const useThreadMock = jest.fn()
const useSearchMock = jest.fn()
const useCommentsMock = jest.fn()
const useRedditMock = jest.fn(() => ({
  useThread: useThreadMock,
  useSearch: useSearchMock,
  useComments: useCommentsMock
}))

const mountReddit = mountWithSuspense(Reddit)

// used for cleaning between tests
// eslint-disable-next-line no-unused-vars
let component
describe('Reddit component', () => {
  afterEach(() => {
    jest.clearAllMocks()
    component = undefined
  })

  it('obtains authenticated Reddit functionality hooks from redditService hook', async () => {
    component = await mountReddit({
      useReddit: useRedditMock,
      redditService: redditServiceMock
    })

    expect(useRedditMock.mock.calls).toEqual([[redditServiceMock]])
    expect(useThreadMock.mock.calls).toEqual([])
    expect(useSearchMock.mock.calls).toEqual([])
    expect(useCommentsMock.mock.calls).toEqual([])
  })

  it('pass Reddit functionalities to default slot', async () => {
    component = await mountReddit({
      useReddit: useRedditMock,
      redditService: redditServiceMock
    }, {
      default: ({ useThread, useSearch, useComments }) => {
        useThread('T')
        useSearch('S')
        useComments('C')
      }
    })

    expect(useRedditMock.mock.calls).toEqual([[redditServiceMock]])
    expect(useThreadMock.mock.calls).toEqual([['T']])
    expect(useSearchMock.mock.calls).toEqual([['S']])
    expect(useCommentsMock.mock.calls).toEqual([['C']])
  })
})
