import redditService, {
  commentsUrl,
  GET_TOKEN_URL,
  mapChildrenData,
  mapRepliesData, newPostsUrl,
  REDDIT_ACCESS_TOKEN_KEY, REDDIT_API_BASE, searchUrl
} from '@/reddit/reddit.service'
import { headers, urlEncode } from '@/utils/utils'

const noCutOff = x => x
const thread = 'aww123'
const searchPhrase = 'not empty321'
const afterArg = 'lastPostID'
const givenAccessToken = 'TOKEN-123'
const postId = 'post-321'
const redditServiceMethodsReturningSamePostEntity = [
  { methodName: 'newPosts', urlCreator: newPostsUrl, args: [afterArg, thread] },
  { methodName: 'search', urlCreator: searchUrl, args: [afterArg, thread, searchPhrase] }
]

describe('Reddit service', () => {
  afterEach(() => {
    jest.clearAllMocks()
    delete global.fetch
  })

  it('getToken', async () => {
    const expectedAccessToken = 'TOKEN-123'
    const fetchMock = Promise.resolve({
      json: () => Promise.resolve({
        [REDDIT_ACCESS_TOKEN_KEY]: expectedAccessToken
      })
    })
    global.fetch = jest.fn(() => fetchMock)

    const accessToken = await redditService.getToken()

    expect(accessToken).toBe(expectedAccessToken)
    expect(global.fetch).toBeCalledTimes(1)
    expect(global.fetch).toHaveBeenCalledWith(GET_TOKEN_URL, {
      method: 'post',
      body: urlEncode({
        grant_type: 'client_credentials'
      }),
      headers: headers()
    })
  })

  describe('posts entity endpoints', () => {
    redditServiceMethodsReturningSamePostEntity.forEach(({
      methodName,
      args,
      urlCreator
    }) => {
      it(methodName + ' redditService method', async () => {
        const postsDataSuccess = {
          data: {
            children: [
              {
                data: {
                  id: 'we care',
                  author: 'that`s author - we care',
                  title: 'that`s title - we care about',
                  thumbnail: 'we love pictures - we care ',
                  created_utc: 'cannot be undone - we care',
                  num_comments: 'if to show comments toggle - we care',
                  selftext: 'this is like body - we care',
                  body: 'link doesnt have it - we drop',
                  created: 'we know when, we dont know where - we drop'
                }
              },
              {
                data: {
                  id: 'just in case'
                }
              }
            ],
            after: 'expectedAfterValue',
            before: 'previousPageWeDontNeed'
          },
          nothing: 'special'
        }
        const expectedPostsModel = {
          children: [
            {
              id: 'we care',
              author: 'that`s author - we care',
              title: 'that`s title - we care about',
              thumbnail: 'we love pictures - we care ',
              created_utc: 'cannot be undone - we care',
              num_comments: 'if to show comments toggle - we care',
              selftext: 'this is like body - we care'
            },
            {
              id: 'just in case'
            }
          ],
          after: 'expectedAfterValue'
        }
        const fetchMock = Promise.resolve({
          json: () => Promise.resolve(postsDataSuccess)
        })
        global.fetch = jest.fn(() => fetchMock)
        const [after, ...firstPageArgs] = args

        const firstPage = await redditService[methodName](givenAccessToken)(...firstPageArgs)

        expect(firstPage).toStrictEqual(expectedPostsModel)
        expect(global.fetch).toBeCalledTimes(1)
        expect(global.fetch).toHaveBeenCalledWith(urlCreator(...firstPageArgs), {
          method: 'get',
          headers: headers(givenAccessToken)
        })

        const secondPage = await redditService[methodName](givenAccessToken)(...firstPageArgs, after)

        expect(secondPage).toStrictEqual(expectedPostsModel)
        expect(global.fetch).toBeCalledTimes(2)
        expect(global.fetch).toHaveBeenLastCalledWith(urlCreator(...firstPageArgs, after), {
          method: 'get',
          headers: headers(givenAccessToken)
        })
      })
    })
  })

  describe('comments entity', () => {
    it('comments redditService method', async () => {
      const commentsDataSuccess = [
        {
          data: {
            postThatCommentsWasAddedTo: 'dont need that'
          }
        },
        {
          data: {
            children: [
              {
                data: {
                  id: 'we care',
                  author: 'that`s author - we care',
                  body: 'this time - ne need it',
                  created_utc: 'cannot be undone - we care',
                  url: 'its comment, so no pictures :( - we drop',
                  created: 'we know when, we dont know where - we drop',
                  replies: {
                    data: {
                      children: [
                        {
                          data: {
                            id: 'always needed',
                            otherData: 'otherData2',
                            replies: ''
                          }
                        }
                      ]
                    }
                  }
                }
              }
            ]
          }
        }
      ]
      const expectedCommentsModel = [
        {
          id: 'we care',
          author: 'that`s author - we care',
          body: 'this time - ne need it',
          created_utc: 'cannot be undone - we care',
          replies: [
            {
              id: 'always needed',
              replies: []
            }
          ]
        }
      ]
      const fetchMock = Promise.resolve({
        json: () => Promise.resolve(commentsDataSuccess)
      })
      global.fetch = jest.fn(() => fetchMock)

      const comments = await redditService.comments(givenAccessToken)(thread, postId)

      expect(comments).toStrictEqual(expectedCommentsModel)
      expect(global.fetch).toBeCalledTimes(1)
      expect(global.fetch).toHaveBeenCalledWith(commentsUrl(thread, postId), {
        method: 'get',
        headers: headers(givenAccessToken)
      })
    })
  })

  describe('url creators', () => {
    it('newPostsUrl', () => {
      expect(newPostsUrl(thread))
        .toBe(REDDIT_API_BASE + thread + '/new.json')
      expect(newPostsUrl(thread, afterArg))
        .toBe(REDDIT_API_BASE + thread + '/new.json?after=' + afterArg)
    })

    it('searchUrl', () => {
      expect(searchUrl(thread, searchPhrase))
        .toBe(REDDIT_API_BASE + thread + '/search.json' +
        '?q=subreddit:' + thread + ' ' + searchPhrase)
      expect(searchUrl(thread, searchPhrase, afterArg))
        .toBe(REDDIT_API_BASE + thread + '/search.json' +
        '?q=subreddit:' + thread + ' ' + searchPhrase + '&after=' + afterArg)
    })

    it('commentsUrl', () => {
      expect(commentsUrl(thread, postId))
        .toBe(REDDIT_API_BASE + thread + '/comments/' + postId + '.json')
    })
  })

  it('mapChildrenData', () => {
    const testIn = {
      kind: 'not important',
      children: [{
        kind: 'still not really',
        data: {
          all: 'is',
          taken: 'from',
          here: true
        }
      }],
      after: 'matters'
    }
    const testOut = {
      children: [
        {
          all: 'is',
          taken: 'from',
          here: true
        }
      ],
      after: 'matters'
    }

    expect(mapChildrenData(noCutOff)(testIn)).toStrictEqual(testOut)
  })

  it('mapRepliesData', () => {
    const testIn = {
      children: [
        {
          data: {
            someData: 'someData1',
            replies: {
              data: {
                children: [
                  {
                    data: {
                      otherData: 'otherData2',
                      replies: {
                        data: {
                          children: [
                            {
                              data: {
                                soDeep: 'soDeep3',
                                replies: {
                                  data: {
                                    children: [
                                      {
                                        data: {
                                          low: 'low4',
                                          replies: ''
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                ]
              }
            }
          }
        }
      ]
    }

    const testOut = [
      {
        someData: 'someData1',
        replies: [
          {
            otherData: 'otherData2',
            replies: [
              {
                soDeep: 'soDeep3',
                replies: [
                  {
                    low: 'low4',
                    replies: []
                  }
                ]
              }
            ]
          }
        ]
      }
    ]

    expect(mapRepliesData(noCutOff)(testIn)).toStrictEqual(testOut)
  })
})
