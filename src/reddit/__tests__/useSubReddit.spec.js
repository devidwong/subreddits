import { useSubReddit } from '@/reddit/useSubReddit'
import { aPost } from '../../../test/utils'

const createLoadMock = (posts, after) => jest.fn(() => Promise.resolve({
  children: posts,
  after
}))
const after = 'lastPostId'
const threadName = 'awwSub'
const setLoadingMock = jest.fn()
const loadMoreWrapper = jest.fn((loadMore) => loadMore)
describe('Subreddit posts provider', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  it('loading posts', async () => {
    const posts = [
      aPost().withId(2).by('ben').build(),
      aPost().withId(3).by('ann').build()
    ]
    const loadMock = createLoadMock(posts, after)
    const { load, threadData } = useSubReddit(loadMock)(setLoadingMock, loadMoreWrapper)

    const loadMore = await load(threadName)

    expect(loadMock.mock.calls).toEqual([[threadName]])
    expect(threadData.children).toEqual(posts)
    expect(setLoadingMock.mock.calls).toEqual([[true], [false]])
    expect(loadMoreWrapper).toHaveBeenCalled()
    jest.clearAllMocks()

    await loadMore()
    await loadMore() // throttled - second call do nothing

    expect(loadMock.mock.calls).toEqual([[threadName, after]])
    expect(threadData.children).toEqual([...posts, ...posts])
    expect(setLoadingMock.mock.calls).toEqual([[true], [false]])
    expect(loadMoreWrapper).not.toHaveBeenCalled()
  })
})
