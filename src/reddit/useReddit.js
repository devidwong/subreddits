import { useRedditSearch } from '@/reddit/useRedditSearch'
import { useSubReddit } from '@/reddit/useSubReddit'
import { useRedditComments } from '@/reddit/useRedditComments'

export const useReddit = async ({
  getToken,
  newPosts,
  search,
  comments
}) => {
  const redditToken = await getToken()
  const useThread = useSubReddit(newPosts(redditToken))
  const useSearch = useRedditSearch(search(redditToken))
  const useComments = useRedditComments(comments(redditToken))
  return {
    useThread,
    useSearch,
    useComments
  }
}
