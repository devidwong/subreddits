import { composeAll, get, getData, getDesiredKeysOnly, headers, toJSON, urlEncode } from '@/utils/utils'

export const REDDIT_ACCESS_TOKEN_KEY = 'access_token'
export const GET_TOKEN_URL = 'https://www.reddit.com/api/v1/access_token'
export const REDDIT_API_BASE = 'https://oauth.reddit.com/r/'

export const newPostsUrl = (thread, after) => REDDIT_API_BASE + thread + '/new.json' +
  (after ? '?after=' + after : '')
export const searchUrl = (thread, phrase, after) => REDDIT_API_BASE + thread + '/search.json' +
  '?q=subreddit:' + thread + ' ' + phrase +
  (after ? '&after=' + after : '')
export const commentsUrl = (thread, linkId) => REDDIT_API_BASE + thread + '/comments/' + linkId + '.json'

const REDDIT_POST_ENTITY_KEYS = [
  'id', 'author', 'title', 'created_utc', 'num_comments', 'thumbnail', 'selftext'
]
const REDDIT_COMMENT_ENTITY_KEYS = [
  'id', 'author', 'body', 'created_utc'
]

const redditService = {
  getToken: () => fetch(
    GET_TOKEN_URL, {
      method: 'post',
      body: urlEncode({
        grant_type: 'client_credentials'
      }),
      headers: headers()
    })
    .then(toJSON)
    .then(get(REDDIT_ACCESS_TOKEN_KEY)),
  newPosts: (accessToken) =>
    (thread, after) => fetch(newPostsUrl(thread, after), {
      method: 'get',
      headers: headers(accessToken)
    })
      .then(toJSON)
      .then(redditPostListingToModel(REDDIT_POST_ENTITY_KEYS)),
  search: (accessToken) =>
    (thread, phrase, after) => fetch(searchUrl(thread, phrase, after), {
      method: 'get',
      headers: headers(accessToken)
    })
      .then(toJSON)
      .then(redditPostListingToModel(REDDIT_POST_ENTITY_KEYS)),
  comments: (accessToken) =>
    (thread, linkId) => fetch(commentsUrl(thread, linkId), {
      method: 'get',
      headers: headers(accessToken)
    })
      .then(toJSON)
      .then(redditCommentListingToModel(REDDIT_COMMENT_ENTITY_KEYS))
}

export default redditService

export const mapChildrenData = (cutOffFunction) => ({ children, after }) => ({
  children: children.map(getData).map(cutOffFunction),
  after
})

export const mapRepliesData = (cutOffFunction) => ({ children }) => {
  const replies = (cutOffFunction) => ({ data }) => ({
    ...cutOffFunction(data),
    replies: data.replies
      ? data.replies.data.children.map(replies(cutOffFunction))
      : []
  })
  return children.map(replies(cutOffFunction))
}

export const redditPostListingToModel = (ENTITY_KEYS) => composeAll(
  mapChildrenData(
    getDesiredKeysOnly(ENTITY_KEYS)
  ),
  getData
)

export const redditCommentListingToModel = (ENTITY_KEYS) => composeAll(
  mapRepliesData(
    getDesiredKeysOnly(ENTITY_KEYS)
  ),
  getData,
  get(1)
)
