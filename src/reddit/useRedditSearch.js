import { reactive } from 'vue'
import { throttle } from '@/utils/throttle'

export const useRedditSearch = (service) => (setLoading, loadMoreWrapper) => {
  const searchResults = reactive({
    children: [],
    after: ''
  })
  const search = async (thread, phrase) => {
    emptySearch()
    setLoading(true)
    addSearchResults(await service(thread, phrase))
    setLoading(false)
    return loadMoreWrapper(throttle(searchMore(thread, phrase), 1500))
  }
  const searchMore = (thread, phrase) => async () => {
    setLoading(true)
    addSearchResults(await service(thread, phrase, searchResults.after))
    setLoading(false)
  }
  const addSearchResults = ({ children, after }) => {
    searchResults.children = [
      ...searchResults.children,
      ...children
    ]
    searchResults.after = after
  }
  const emptySearch = () => {
    searchResults.children = []
    searchResults.after = ''
  }
  return {
    searchResults,
    search
  }
}
