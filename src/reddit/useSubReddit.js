import { reactive } from 'vue'
import { throttle } from '@/utils/throttle'

export const useSubReddit = (service) => (setLoading, loadMoreWrapper) => {
  const threadData = reactive({
    children: [],
    after: ''
  })
  const load = async (threadName) => {
    emptyPosts()

    setLoading(true)
    addPosts(await service(threadName))
    setLoading(false)

    return loadMoreWrapper(throttle(loadMore(threadName), 1500))
  }
  const loadMore = (threadName) => async () => {
    setLoading(true)
    addPosts(await service(threadName, threadData.after))
    setLoading(false)
  }
  const addPosts = ({ children, after }) => {
    threadData.children = [
      ...threadData.children,
      ...children
    ]
    threadData.after = after
  }
  const emptyPosts = () => {
    threadData.children = []
    threadData.after = ''
  }

  return {
    threadData,
    load
  }
}
