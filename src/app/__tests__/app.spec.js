import { flushPromises, mount } from '@vue/test-utils'
import AppComponent from '@/app/app'
import redditService, { REDDIT_ACCESS_TOKEN_KEY } from '@/reddit/reddit.service'
import { click, element, elements, fetchMockFactory, text, type } from '../../../test/utils'

const postsDataSuccess = {
  data: {
    children: [
      {
        data: {
          id: '123',
          author: 'TheAuthor',
          title: 'TheTitle',
          url: 'https://example.com/img.png',
          created_utc: 1604358526,
          num_comments: 0
        }
      },
      {
        data: {
          id: '1234',
          author: 'TheAuthor1',
          title: 'TheTitle1',
          url: 'https://example.com/img1.png',
          created_utc: 1604357526,
          num_comments: 3
        }
      }
    ],
    after: '1234',
    before: ''
  },
  kind: 'Listing'
}
const commentsDataSuccess = [
  {
    data: { // already have
      id: '1234',
      postThatCommentsWasAddedTo: 'so we drop'
    }
  },
  {
    data: {
      children: [
        {
          data: {
            id: 'com-1',
            author: 'Author-com-1',
            body: 'Body-com-1',
            created_utc: 1604228526,
            replies: ''
          }
        },
        {
          data: {
            id: 'com-2',
            author: 'Author-com-2',
            body: 'Body-com-2',
            created_utc: 1604228527,
            replies: {
              data: {
                children: [
                  {
                    data: {
                      id: 'com-21',
                      author: 'Author-com-21',
                      body: 'Body-com-21',
                      created_utc: 1604117529,
                      replies: ''
                    }
                  }
                ]
              }
            }
          }
        }
      ]
    }
  }
]

let component
describe('Reddit viewer app integration', () => {
  afterEach(() => {
    jest.clearAllMocks()
    component = undefined
    delete global.fetch
  })
  beforeEach(async () => {
    await fakeGetToken()
    global.fetch = fetchMockFactory(postsDataSuccess)
    component = await mountApp({
      threads: ['thread1', 'thread2', 'thread3']
    })
  })

  it('instantly show first thread with comments', async () => {
    const { post2, toggleComments } = expectPostsToBeDisplayed()

    global.fetch = fetchMockFactory(commentsDataSuccess)

    await expectCommentsToBeDisplayed(post2, toggleComments)
  })

  it('search in thread with loading message', async () => {
    const searchDataSuccess = {
      data: {
        children: [
          {
            data: {
              id: '321',
              author: 'TheAuthorSearched',
              title: 'TheTitleFound',
              url: 'https://example.com/lookAfter.png', // TODO
              created_utc: 1604358526,
              num_comments: 0
            }
          },
          {
            data: {
              id: '3214',
              author: 'TheAuthor1Searched',
              title: 'TheTitle1Found',
              url: 'https://example.com/lookAfter1.png',
              created_utc: 1604357526,
              num_comments: 3
            }
          }
        ],
        after: '1234',
        before: ''
      },
      kind: 'Listing'
    }
    await type('how to be truthy?').in('.search-phrase-input', component)
    global.fetch = fetchMockFactory(searchDataSuccess)

    expect(element('.apply-search', component).exists()).toBeTruthy()
    await click().in('.apply-search', component)

    await expectLoadingMessageToBe('Loading search results in /r/thread1 for phrase "how to be truthy?"...')
    const { searchResult2, toggleComments } = await expectSearchResultsToBeDisplayed()
    global.fetch = fetchMockFactory(commentsDataSuccess)
    await expectCommentsToBeDisplayed(searchResult2, toggleComments)
  })

  it('change thread with loading message', async () => {
    const postsInThread2DataSuccess = {
      data: {
        children: [
          {
            data: {
              id: '12354',
              author: 'TheAuthorThread2',
              title: 'TheTitleThread2',
              url: 'https://example.com/img.png',
              created_utc: 1604358526,
              num_comments: 0
            }
          },
          {
            data: {
              id: '123476',
              author: 'TheAuthor1Thread2',
              title: 'TheTitle1Thread2',
              url: 'https://example.com/img1.png',
              created_utc: 1604357526,
              num_comments: 3
            }
          }
        ],
        after: '1234',
        before: ''
      },
      kind: 'Listing'
    }
    global.fetch = fetchMockFactory(postsInThread2DataSuccess)

    await click().in('.go-to-thread-thread2', component)

    await expectLoadingMessageToBe('Loading subreddit /r/thread2...')
    const { post2, toggleComments } = expectThread2PostsToBeDisplayed()
    global.fetch = fetchMockFactory(commentsDataSuccess)
    await expectCommentsToBeDisplayed(post2, toggleComments)
  })

  const expectPostsToBeDisplayed = () => {
    const posts = elements('.app .reddit .posts .post', component)
    const [post1, post2] = posts
    expect(redditService.getToken).toHaveBeenCalled()
    expect(posts.length).toBe(2)
    expect(text().on('h2.post__title', post1)).toBe('TheTitle')
    expect(text().on('h2.post__title', post2)).toBe('TheTitle1')
    expect(text().on('.credit .credit__author', post1)).toBe('by TheAuthor')
    expect(text().on('.credit .credit__author', post2)).toBe('by TheAuthor1')
    expect(text().on('.credit .credit__created', post1)).toBe('@ 2020-11-02 23:08:46')
    expect(text().on('.credit .credit__created', post2)).toBe('@ 2020-11-02 22:52:06')
    expect(text().on('.post__comment-count', post1)).toBe('0 comments posted')
    expect(text().on('.post__comment-count', post2)).toBe('3 comments posted')
    expect(element('.comments .comments__toggle button', post1).exists()).toBeFalsy()
    const toggleComments = element('.comments .comments__toggle button', post2)
    expect(toggleComments.text()).toBe('toggle comments')
    return {
      post2,
      toggleComments
    }
  }

  const expectThread2PostsToBeDisplayed = () => {
    const posts = elements('.app .reddit .posts .post', component)
    const [post1, post2] = posts
    expect(redditService.getToken).toHaveBeenCalled()
    expect(posts.length).toBe(2)
    expect(text().on('h2.post__title', post1)).toBe('TheTitleThread2')
    expect(text().on('h2.post__title', post2)).toBe('TheTitle1Thread2')
    expect(text().on('.credit .credit__author', post1)).toBe('by TheAuthorThread2')
    expect(text().on('.credit .credit__author', post2)).toBe('by TheAuthor1Thread2')
    expect(text().on('.credit .credit__created', post1)).toBe('@ 2020-11-02 23:08:46')
    expect(text().on('.credit .credit__created', post2)).toBe('@ 2020-11-02 22:52:06')
    expect(element('.comments .comments__toggle button', post1).exists()).toBeFalsy()
    const toggleComments = element('.comments .comments__toggle button', post2)
    expect(toggleComments.text()).toBe('toggle comments')
    return {
      post2,
      toggleComments
    }
  }

  const expectSearchResultsToBeDisplayed = () => {
    const searchResults = elements('.app .reddit .posts .post', component)
    const [searchResult1, searchResult2] = searchResults
    expect(redditService.getToken).toHaveBeenCalled()
    expect(searchResults.length).toBe(2)
    expect(text().on('h2.post__title', searchResult1)).toBe('TheTitleFound')
    expect(text().on('h2.post__title', searchResult2)).toBe('TheTitle1Found')
    expect(text().on('.credit .credit__author', searchResult1)).toBe('by TheAuthorSearched')
    expect(text().on('.credit .credit__author', searchResult2)).toBe('by TheAuthor1Searched')
    expect(text().on('.credit .credit__created', searchResult1)).toBe('@ 2020-11-02 23:08:46')
    expect(text().on('.credit .credit__created', searchResult2)).toBe('@ 2020-11-02 22:52:06')
    expect(element('.comments .comments__toggle button', searchResult1).exists()).toBeFalsy()
    const toggleComments = element('.comments .comments__toggle button', searchResult2)
    expect(toggleComments.text()).toBe('toggle comments')
    return {
      searchResult2,
      toggleComments
    }
  }

  const expectLoadingMessageToBe = async (message) => {
    expect(text().on('.posts .loading', component))
      .toBe(message)
    await flushPromises()
    expect(element('.posts .loading', component).exists()).toBeFalsy()
  }

  const expectCommentsToBeDisplayed = async (post, toggler) => {
    await click().element(toggler)

    expect(text().on('.comments .loading', post)).toBe('Loading comments...')

    await flushPromises()

    const commentsFirstLevel = elements('.comments .comments__list > .comment', post)
    const [comment1, comment2] = commentsFirstLevel
    expect(commentsFirstLevel.length).toBe(2)
    expect(text().on('.comment__credit.credit .credit__author', comment1)).toBe('by Author-com-1')
    expect(text().on('.comment__credit.credit .credit__author', comment2)).toBe('by Author-com-2')
    expect(text().on('.comment__body', comment1)).toBe('Body-com-1')
    expect(text().on('.comment__body', comment2)).toBe('Body-com-2')
    expect(text().on('.comment__credit.credit .credit__created', comment1)).toBe('@ 2020-11-01 11:02:06')
    expect(text().on('.comment__credit.credit .credit__created', comment2)).toBe('@ 2020-11-01 11:02:07')

    const replyToComment2 = element('.comments .comments__list > .comment > .comment', post)
    expect(text().on('.comment__credit.credit .credit__author', replyToComment2)).toBe('by Author-com-21')
    expect(text().on('.comment__body', replyToComment2)).toBe('Body-com-21')
    expect(text().on('.comment__credit.credit .credit__created', replyToComment2)).toBe('@ 2020-10-31 04:12:09')

    await click().element(toggler)

    expect(element('.comments .comments__list > .comment', post).exists()).toBeFalsy()
  }

  const fakeGetToken = async () => {
    jest.spyOn(redditService, 'getToken').mockImplementation(() => Promise.resolve({
      json: () => Promise.resolve({
        [REDDIT_ACCESS_TOKEN_KEY]: 'token'
      })
    }))
    await flushPromises()
  }

  const mountApp = async () => {
    const wrapper = mount(AppComponent, {
      props: {
        threads: ['thread1', 'thread2', 'thread3']
      }
    })
    await flushPromises()
    return wrapper
  }
})
