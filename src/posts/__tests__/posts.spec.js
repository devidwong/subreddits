import { mount } from '@vue/test-utils'
import PostsComponent from '@/posts/posts'
import { ref } from 'vue'
import { element, elements, text, change, aPost, click } from '../../../test/utils'

const loadMoreSearchMock = jest.fn()
const loadMoreThreadMock = jest.fn()
const loadThenMock = jest.fn((infiniteScroll) => infiniteScroll(loadMoreThreadMock))
const searchThenMock = jest.fn((infiniteScroll) => infiniteScroll(loadMoreSearchMock))
const loadMock = jest.fn(() => ({ then: loadThenMock }))
const searchMock = jest.fn(() => ({ then: searchThenMock }))
const useThreadMock = jest.fn(() => ({
  load: loadMock,
  threadData: {
    children: []
  }
}))
const useSearchMock = jest.fn(() => ({
  search: searchMock,
  searchResults: {
    children: []
  }
}))
const useThreadWithData = (children) => () => ({
  load: loadMock,
  threadData: {
    children
  }
})
const useSearchWithData = (children) => () => ({
  search: searchMock,
  searchResults: {
    children
  }
})

const infiniteScrollMock = jest.fn()
const makeScrollHandlerMock = jest.fn()
const useInfiniteScrollMock = () => ({
  infiniteScroll: infiniteScrollMock,
  makeScrollHandler: makeScrollHandlerMock
})
const setLoadingMock = jest.fn()
const createUseLoadingMock = (loadingState) => () => ({
  loading: ref(loadingState),
  setLoading: setLoadingMock
})

const initialThread = 'aww'
const newThread = 'memes'
const initialSearchPhraseApplied = ''
const notEmptySearchPhraseApplied = 'search phrase'
const mountPosts = (props, slots) => mount(PostsComponent, { props, slots })

let component
describe('Posts', () => {
  afterEach(() => {
    jest.clearAllMocks()
    component = undefined
  })

  it('loads thread at start', async () => {
    component = await mountPosts({
      thread: initialThread,
      useThread: useThreadMock,
      useSearch: useSearchMock
    })

    expect(loadMock.mock.calls).toEqual([[initialThread]])
    expect(searchMock.mock.calls).toEqual([])
  })

  describe('watching thread and searchPhraseApplied', () => {
    it('call load prop on thread change', async () => {
      const thread = ref(initialThread)
      component = await mountPosts({
        thread,
        searchPhraseApplied: '',
        useThread: useThreadMock,
        useSearch: useSearchMock
      })

      await change(thread).to(newThread)

      expect(loadMock.mock.calls).toEqual([[initialThread], [newThread]])
      expect(searchMock.mock.calls).toEqual([])
    })

    it('call search prop on searchPhraseApplied change', async () => {
      const newSearchPhraseApplied = 'memes'
      const searchPhraseApplied = ref(initialSearchPhraseApplied)
      component = await mountPosts({
        thread: initialThread,
        searchPhraseApplied,
        useThread: useThreadMock,
        useSearch: useSearchMock
      })

      await change(searchPhraseApplied).to(newSearchPhraseApplied)

      expect(loadMock.mock.calls).toEqual([[initialThread]])
      expect(searchMock.mock.calls).toEqual([[initialThread, newSearchPhraseApplied]])
    })

    it('call load prop on searchPhraseApplied reset', async () => {
      const resetSearchPhraseApplied = ''
      const searchPhraseApplied = ref(notEmptySearchPhraseApplied)
      component = await mountPosts({
        thread: initialThread,
        searchPhraseApplied,
        useThread: useThreadMock,
        useSearch: useSearchMock
      })

      await change(searchPhraseApplied).to(resetSearchPhraseApplied)

      expect(loadMock.mock.calls).toEqual([[initialThread], [initialThread]])
      expect(searchMock.mock.calls).toEqual([])
    })

    // That's why we reset searchPhraseApplied before switching threads
    //  - otherwise it'll load searchResults from new thread
    it('call search prop on thread change if searchPhraseApplied is not empty', async () => {
      const thread = ref(initialThread)
      component = await mountPosts({
        thread,
        searchPhraseApplied: notEmptySearchPhraseApplied,
        useThread: useThreadMock,
        useSearch: useSearchMock
      })

      await change(thread).to(newThread)

      expect(loadMock.mock.calls).toEqual([[initialThread]])
      expect(searchMock.mock.calls).toEqual([[newThread, notEmptySearchPhraseApplied]])
    })
  })

  describe('infiniteScroll', () => {
    it('call infiniteScroll', async () => {
      component = await mountPosts({
        thread: initialThread,
        useThread: useThreadMock,
        useSearch: useSearchMock,
        useInfiniteScroll: useInfiniteScrollMock
      })

      expect(loadThenMock.mock.calls).toEqual([[infiniteScrollMock]])
      expect(infiniteScrollMock.mock.calls).toEqual([[loadMoreThreadMock]])
      expect(searchMock.mock.calls).toEqual([])
    })

    it('call infiniteScroll on load new thread', async () => {
      const thread = ref(initialThread)
      component = await mountPosts({
        thread,
        searchPhraseApplied: '',
        useThread: useThreadMock,
        useSearch: useSearchMock,
        useInfiniteScroll: useInfiniteScrollMock
      })

      await change(thread).to(newThread)

      expect(loadThenMock.mock.calls).toEqual([[infiniteScrollMock], [infiniteScrollMock]])
      expect(infiniteScrollMock.mock.calls).toEqual([[loadMoreThreadMock], [loadMoreThreadMock]])
      expect(searchMock.mock.calls).toEqual([])
    })

    it('call infiniteScroll on load new search', async () => {
      const searchPhraseApplied = ref(initialSearchPhraseApplied)
      component = await mountPosts({
        thread: initialThread,
        searchPhraseApplied,
        useThread: useThreadMock,
        useSearch: useSearchMock,
        useInfiniteScroll: useInfiniteScrollMock
      })

      await change(searchPhraseApplied).to(notEmptySearchPhraseApplied)

      expect(loadThenMock.mock.calls).toEqual([[infiniteScrollMock]])
      expect(searchThenMock.mock.calls).toEqual([[infiniteScrollMock]])
      expect(infiniteScrollMock.mock.calls).toEqual([[loadMoreThreadMock], [loadMoreSearchMock]])
    })
  })

  describe('loading', () => {
    it('passes setLoading to useThread and useSearch', async () => {
      component = await mountPosts({
        thread: initialThread,
        useThread: useThreadMock,
        useSearch: useSearchMock,
        useLoading: createUseLoadingMock(false),
        useInfiniteScroll: useInfiniteScrollMock
      })

      expect(useThreadMock.mock.calls).toEqual([[setLoadingMock, makeScrollHandlerMock]])
      expect(useSearchMock.mock.calls).toEqual([[setLoadingMock, makeScrollHandlerMock]])
    })

    it('show loading slot on loading', async () => {
      const loadingMessage = 'Spin a round'
      component = await mountPosts({
        thread: initialThread,
        searchPhraseApplied: '',
        useThread: useThreadWithData([]),
        useSearch: useSearchMock,
        useLoading: createUseLoadingMock(true)
      }, {
        loading: loadingMessage
      })

      expect(element('.loading', component).exists()).toBeTruthy()
      expect(text().on('.loading', component)).toBe(loadingMessage)
    })

    it('not show spinner out of loading', async () => {
      component = await mountPosts({
        thread: initialThread,
        searchPhraseApplied: '',
        useThread: useThreadWithData([
          aPost().withId(0).build()
        ]),
        useSearch: useSearchMock,
        useLoading: createUseLoadingMock(false)
      })

      expect(element('.loading', component).exists()).toBeFalsy()
    })
  })

  describe('rendering posts', () => {
    it('lists posts if no searchPhraseApplied', async () => {
      const id = 11
      const author = 'JohnTheAuthor'
      component = await mountPosts({
        searchPhraseApplied: '',
        thread: initialThread,
        useThread: useThreadWithData([
          aPost().withId(id).by(author).build()
        ]),
        useSearch: useSearchWithData([
          aPost().withId(2).build(),
          aPost().withId(3).build()
        ])
      }, {
        default: '<template #default="{ post }">{{ post.author + post.id }}</template>'
      })

      const posts = elements('.post', component)
      expect(posts.length).toBe(1)
      expect(posts[0].text()).toBe(author + id)
    })

    it('lists searchResults if searchPhraseApplied not empty', async () => {
      component = await mountPosts({
        searchPhraseApplied: notEmptySearchPhraseApplied,
        thread: initialThread,
        useThread: useThreadWithData([
          aPost().withId(0).build()
        ]),
        useSearch: useSearchWithData([
          aPost().withId(2).by('ben').build(),
          aPost().withId(3).by('ann').build()
        ])
      }, {
        default: '<template #default="{ post }">{{ post.author }}</template>'
      })

      const posts = component.findAll('.post')
      expect(posts.length).toBe(2)
      expect(posts[0].text()).toBe('ben')
      expect(posts[1].text()).toBe('ann')
    })
  })

  describe('empty state', () => {
    it('show empty state slot if searchPhraseApplied empty, no posts and loading is false', async () => {
      const emptyMessage = 'Pretty empty'
      component = await mountPosts({
        thread: initialThread,
        searchPhraseApplied: '',
        useThread: useThreadWithData([]),
        useSearch: useSearchWithData([
          aPost().withId(0).build()
        ]),
        useLoading: createUseLoadingMock(false)
      }, {
        empty: emptyMessage
      })

      expect(elements('.post', component).length).toBe(0)
      expect(text().on('.posts__empty', component)).toBe(emptyMessage)
    })

    it('show empty state for searchResults if searchPhraseApplied not empty, no posts and loading is false', async () => {
      component = await mountPosts({
        thread: initialThread,
        searchPhraseApplied: notEmptySearchPhraseApplied,
        useThread: useThreadWithData([
          aPost().withId(0).build()
        ]),
        useSearch: useSearchWithData([]),
        useLoading: createUseLoadingMock(false)
      })

      expect(elements('.post', component).length).toBe(0)
      expect(element('.posts__empty', component).exists()).toBeTruthy()
    })
  })

  describe('single post', () => {
    it('select single post - not bubble if (postIndex % 2) === 1', async () => {
      component = await mountPosts({
        searchPhraseApplied: '',
        thread: initialThread,
        useThread: useThreadWithData([
          aPost().withId(1).by('author1').build(),
          aPost().withId(2).by('author2').build()
        ]),
        useSearch: useSearchMock
      }, {
        default: '<template #default="{ post }">{{ post.author + post.id }}</template>'
      })

      await click().in('#post-id-1', component)

      expect(element('#post-id-1', component).classes()).toContain('post--single')
      expect(element('#post-id-2', component).classes()).not.toContain('post--single')
      expect(elements('.post', component).map(e => e.text())).toEqual([
        'author11',
        'author22'
      ])
    })

    it('select single post - bubble if (postIndex % 2) === 0', async () => {
      component = await mountPosts({
        searchPhraseApplied: '',
        thread: initialThread,
        useThread: useThreadWithData([
          aPost().withId(1).by('author1').build(),
          aPost().withId(2).by('author2').build()
        ]),
        useSearch: useSearchMock
      }, {
        default: '<template #default="{ post }">{{ post.author + post.id }}</template>'
      })

      await click().in('#post-id-2', component)

      expect(element('#post-id-2', component).classes()).toContain('post--single')
      expect(element('#post-id-1', component).classes()).not.toContain('post--single')
      expect(elements('.post', component).map(e => e.text())).toEqual([
        'author22',
        'author11'
      ])
    })
  })
})
