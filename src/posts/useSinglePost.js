import { ref } from 'vue'

export const useSinglePost = () => {
  const singlePostId = ref(null)
  const isPostSingle = (id) => singlePostId.value === id
  const isPostSingleSet = () => !!singlePostId.value
  const goToList = () => {
    singlePostId.value = null
  }
  const scrollToPost = (id) => () => {
    const selectedPost = document.getElementById('post-id-' + id)
    document.documentElement.scrollTop = selectedPost.offsetTop - 16
  }
  const goToSinglePost = (id) => {
    singlePostId.value = id
    setTimeout(scrollToPost(id), 90)
  }
  const bubblePostOnceSelected = (acc, post, index) => {
    acc[index] = post
    if (isPostSingle(post.id) && (index % 2)) {
      [acc[index - 1], acc[index]] = [acc[index], acc[index - 1]]
    }
    return acc
  }
  return {
    isPostSingle,
    isPostSingleSet,
    goToList,
    goToSinglePost,
    bubblePostOnceSelected
  }
}
