export const headers = (token) => {
  const REDDIT_APP_CREDENTIALS = 'vGXHb68jbtCeSQ:Q3RSMJzYQYq9MTAR9Rf1NigOBvw'
  const Authorization = token
    ? 'Bearer ' + token
    : 'Basic ' + btoa(REDDIT_APP_CREDENTIALS)
  return new Headers({
    ...commonHeaders,
    Authorization
  })
}

export const urlEncode = (data) => Object.entries(data)
  .map(([key, value]) => key + '=' + encodeURIComponent(value))
  .join('&')

const commonHeaders = {
  'User-agent': 'web:com.devidwong.reddit-viewer:v0.0.1 (by /u/devidwong)',
  'Content-type': 'application/x-www-form-urlencoded'
}
export const formatDate = (date) => {
  return new Date(date * 1000)
    .toISOString()
    .replace('T', ' ')
    .slice(0, -5)
}
export const toJSON = (response) => response.json()
export const get = (key) => (object) => object[key]
export const getDesiredKeysOnly = (keys) => (object) => Object.entries(object)
  .reduce((acc, [key, value]) => keys.includes(key)
    ? ({ ...acc, [key]: value })
    : acc,
  {})
export const getData = get('data')
export const compose = (f, g) => (...args) => f(g(...args))
export const composeAll = (...fns) => fns.reduce(compose)
