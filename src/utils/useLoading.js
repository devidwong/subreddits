import { ref } from 'vue'

export const useLoading = () => {
  const loading = ref(false)
  const setLoading = (newState) => {
    loading.value = newState
  }
  return {
    loading,
    setLoading
  }
}
