import { reactive, onUnmounted } from 'vue'

export const useInfiniteScroll = () => {
  const current = reactive({
    handler: null
  })
  const makeScrollHandler = (throttledMore) => () => {
    const isBottomReached = (window.innerHeight + Math.ceil(window.pageYOffset)) >= document.body.offsetHeight
    if (isBottomReached) {
      throttledMore()
    }
  }
  const infiniteScroll = (handler) => {
    if (handler === current.handler) {
      return
    }
    if (current.handler) {
      document.removeEventListener('scroll', current.handler)
    }
    document.addEventListener('scroll', handler)
    current.handler = handler
  }
  const removeInfiniteScroll = () => {
    document.removeEventListener('scroll', current.handler)
  }
  onUnmounted(removeInfiniteScroll)
  return {
    infiniteScroll,
    makeScrollHandler
  }
}
