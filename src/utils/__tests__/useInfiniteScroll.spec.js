import { useInfiniteScroll } from '@/utils/useInfiniteScroll'

const loadMoreMock = jest.fn()
const newLoadMoreMock = jest.fn()
describe('useInfiniteScroll', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  it('manage document scroll event listeners', () => {
    jest.spyOn(document, 'addEventListener')
    jest.spyOn(document, 'removeEventListener')
    const { infiniteScroll, makeScrollHandler } = useInfiniteScroll()
    // just to show how to wrap throttled api call in pageBottom detector
    const scrollHandler = makeScrollHandler(loadMoreMock)

    infiniteScroll(scrollHandler)

    expect(document.addEventListener).toHaveBeenCalled()
    expect(document.removeEventListener).not.toHaveBeenCalled()
    jest.clearAllMocks()

    infiniteScroll(scrollHandler) // same handler should do nothing

    expect(document.addEventListener).not.toHaveBeenCalled()
    expect(document.removeEventListener).not.toHaveBeenCalled()

    infiniteScroll(newLoadMoreMock)

    expect(document.addEventListener).toHaveBeenCalled()
    expect(document.removeEventListener).toHaveBeenCalled()
  })
})
