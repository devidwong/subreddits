import { formatDate } from '@/utils/utils'

describe('formatData', () => {
  [
    {
      posixDate: 1604228526,
      expectedString: '2020-11-01 11:02:06'
    },
    {
      posixDate: 1604117529,
      expectedString: '2020-10-31 04:12:09'
    }
  ].forEach(({ posixDate, expectedString }) => {
    it('test for posixDateString ' + posixDate, () => {
      expect(formatDate(posixDate)).toBe(expectedString)
    })
  })
})
