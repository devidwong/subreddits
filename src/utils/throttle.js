export const throttle = (toThrottle, timeout) => {
  let isThrottled = false
  const resetThrottle = () => {
    isThrottled = false
  }
  return () => {
    if (isThrottled) {
      return
    }
    isThrottled = true
    setTimeout(resetThrottle, timeout)
    toThrottle()
  }
}
