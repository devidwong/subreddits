import Comments from '@/comments/comments'
import { mount } from '@vue/test-utils'
import { click, elements, text, element, aComment } from '../../../test/utils'

const toggleCommentsMock = jest.fn()
const useCommentsMock = jest.fn(() => ({
  comments: [],
  toggleComments: toggleCommentsMock,
  loading: false
}))

let component

describe('Comments component', () => {
  afterEach(() => {
    jest.clearAllMocks()
    component = undefined
  })

  it('call useComments functionality hook', async () => {
    component = await mountComments(useCommentsMock)

    expect(useCommentsMock.mock.calls).toEqual([[]])
    expect(toggleCommentsMock.mock.calls).toEqual([])
  })

  it('pass toggleComments method to named slot "toggle"', async () => {
    component = await mountComments(useCommentsMock, {
      toggle: `<template #toggle='{ toggleComments }'>
            <button class="toggle-comments-button" @click='toggleComments'></button>
        </template>`
    })

    await click().in('.comments__toggle button', component)

    expect(useCommentsMock.mock.calls).toEqual([[]])
    expect(toggleCommentsMock).toHaveBeenCalled()
  })

  it('display loading slot if comments still loading', async () => {
    component = await mountComments(createUseComments()
      .notLoaded()
      .build(), {
      loading: `<template #loading>
            Loading test comments...
        </template>`
    })

    expect(text().on('.comments__loading', component)).toEqual('Loading test comments...')
    expect(element('.comments__list', component).exists()).toBeFalsy()
  })

  it('pass comments to default slot', async () => {
    component = await mountComments(createUseComments()
      .loaded()
      .withComments(
        aComment().withId('com1').by('author1').build(),
        aComment().withId('com2').by('author2').build()
      )
      .build(), {
      default: `<template #default='{ comments }'>
            <div class="comment" v-for="comment in comments">{{ comment.id + comment.author }}</div>
        </template>`
    })

    expect(elements('.comments__list .comment', component).length).toBe(2)
    expect(elements('.comments__list .comment', component)[0].text()).toBe('com1author1')
    expect(elements('.comments__list .comment', component)[1].text()).toBe('com2author2')
    expect(element('.comments__loading', component).exists()).toBeFalsy()
  })
})

const mountComments = (useComments, slots) => mount(Comments, {
  props: { useComments },
  slots
})

function createUseComments () {
  const value = {
    useComments: {
      toggleComments: toggleCommentsMock,
      comments: [],
      loading: false
    }
  }
  return {
    withComments (...comments) {
      value.useComments = {
        ...value.useComments,
        comments
      }
      return this
    },
    loaded () {
      value.useComments = {
        ...value.useComments,
        loading: false
      }
      return this
    },
    notLoaded () {
      value.useComments = {
        ...value.useComments,
        loading: true
      }
      return this
    },
    build () {
      return () => value.useComments
    }
  }
}
