import Comment from '@/comments/comment'
import { mount } from '@vue/test-utils'
import { h, defineComponent, markRaw } from 'vue'
import { text } from '../../../test/utils'

const DetailsComponent = () => ({
  consuming: (...commentProperties) => defineComponent({
    props: { comment: Object },
    setup (props) {
      return () => h('div', { class: 'comment-details-component' }, commentProperties
        .map((propertyName) => h('div', { class: propertyName }, props.comment[propertyName])))
    }
  })
})

let component

describe('Comment component', () => {
  afterEach(() => {
    jest.clearAllMocks()
    component = undefined
  })

  it('render details', () => {
    const id = 'a'
    const author = 'john'
    component = mountComment(DetailsComponent()
      .consuming('author', 'id'),
    { id, author })

    expect(text().on('.comment .comment-details-component .id', component)).toBe(id)
    expect(text().on('.comment .comment-details-component .author', component)).toBe(author)
  })

  it('render replies recursively', () => {
    const id = 'a'
    const author = 'john'
    component = mountComment(DetailsComponent()
      .consuming('author', 'id'),
    {
      id,
      author,
      replies: [
        { id: 'a1', author: 'frank' },
        {
          id: 'a2',
          author: 'bob',
          replies: [
            { id: 'b', author: 'alice' }
          ]
        }
      ]
    })

    expect(text().on('.comment .comment-details-component .id', component)).toBe(id)
    expect(text().on('.comment .comment-details-component .author', component)).toBe(author)
    expect(text().on('.comment .comment .comment-details-component .id', component)).toBe('a1')
    expect(text().on('.comment .comment .comment-details-component .author', component)).toBe('frank')
    expect(text().on('.comment .comment + .comment .comment-details-component .id', component)).toBe('a2')
    expect(text().on('.comment .comment + .comment .comment-details-component .author', component)).toBe('bob')
    expect(text().on('.comment .comment .comment .comment-details-component .id', component)).toBe('b')
    expect(text().on('.comment .comment .comment .comment-details-component .author', component)).toBe('alice')
  })
})

const mountComment = (detailsComponent, comment) => mount(Comment, {
  props: {
    detailsComponent: markRaw(detailsComponent),
    comment
  }
})
